</main> <!-- /.main -->
<footer class="footer">
  <div class="footer__copyright">© 2006-2017, SMB24.RU / Портал является собственностью Правительства Красноярского края. Cоздан в рамках краевой целевой программы "Государственная поддержка и развитие малого предпринимательства в Красноярском крае" на 2006-2007гг.</div>
  <a href="#" target="_blank" class="footer__developer">
    <div class="img-wrap"><img src="modules/footer/img/novosoft.jpg" alt=""></div>
    <div class="text">Сделано в ООО "Новый Софт", 2017</div>
  </a>
</footer> <!-- /.footer -->
</section>

<div class="js-page-scroll-to-top img-wrap">
    <img src="img/arrow-up.png" alt="">
  </div>