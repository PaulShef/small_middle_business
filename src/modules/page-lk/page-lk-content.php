<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <span class="breadcrumb-item active">Личный кабинет</span>
</nav>


<div class="lk open-login">
  <div class="lk__toggle_nav">
    <span class="js-login">Вход</span>
    /
    <span class="js-signin">Регистрация</span>
  </div>
  <div class="lk__toggle_content">
    <div class="lk__toggle_content-login">
      <div class="title">Вход для зарегистрированных пользователей</div>
      <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="email">E-mail: <span class="star-required">*</span></label>
            <input type="email" class="form-control" id="email">
          </div>
          <div class="form-group col-md-6">
            <label for="password">Пароль: <span class="star-required">*</span></label>
            <input type="password" class="form-control" id="password">
          </div>
        </div>
        
        <div class="form-footer">
          <a class="link blue" href="#">Забыли пароль?</a>
          <button type="submit" class="btn btn-1 red">Войти <div class="svg-wrap">
            <svg>
              <use xlink:href="#arrow1"></use>
            </svg>
          </div></button>
        </div>
      </form>
      <div class="info">
        <div>Регистрация на портале предоставляет пользователям <strong>следующие возможности</strong>:</div>
        <ul class="ul-mark">
          <li>зарегистрировать компанию в каталоге;</li>
          <li>внести информацию о товарах с возможностью загрузки фото;</li>
          <li>внести информацию об услугах с возможностью загрузки фото;</li>
          <li>создать мини-сайт компании;</li>
          <li>разместить новости компании;</li>
          <li>публиковать статьи;</li>
          <li>загружать прайс-листы;</li>
          <li>публиковать инвестиционные предложения и др.</li>
        </ul>
        <div>Методическое пособие: <a class="link blue" href="">Как предпринимателю начать работать в системе</a></div>
      </div>
    </div>
    <div class="lk__toggle_content-signin">
      <div class="title">Регистрация на портале</div>
      <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="reg-email">E-mail: <span class="star-required">*</span></label>
            <input type="email" class="form-control" id="reg-email">
          </div>
          <div class="form-group col-md-6">
            <label for="reg-name-1">Фамилия: <span class="star-required">*</span></label>
            <input type="text" class="form-control" id="reg-name-1">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="reg-name-2">Имя: <span class="star-required">*</span></label>
            <input type="text" class="form-control" id="reg-name-2">
          </div>
          <div class="form-group col-md-6">
            <label for="reg-name-3">Отчество: <span class="star-required">*</span></label>
            <input type="text" class="form-control" id="reg-name-3">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="reg-password">Пароль: <span class="star-required">*</span></label>
            <input type="password" class="form-control" id="reg-password" aria-describedby="passwordHelpBlock">
            <small id="passwordHelpBlock" class="form-text">
              (до 10 знаков, допускается русский/ английский язык, цифры и др. символы)
            </small>
          </div>
          <div class="form-group col-md-6">
            <label for="reg-password-2">Повторите пароль: <span class="star-required">*</span></label>
            <input type="password" class="form-control" id="reg-password-2">
          </div>
        </div>
        <div class="form-group">
          <div class="form-check">
            <label id="js-reg-check" class="form-check-label">
              <input class="form-check-input" type="checkbox"><strong>Я согласен(а) на обработку персональных данных</strong>, в соответствии с ФЗ «О персональных данных» от 27.07.2006 № 152-ФЗ
            </label>
          </div>
        </div>
        
        <div class="form-footer">
          <small>Поля, отмеченные <span class="star-required">*</span>, обязательны для заполнения</small>
          <button type="submit" class="btn btn-1 red disabled">Зарегистрироваться<div class="svg-wrap">
            <svg>
              <use xlink:href="#arrow1"></use>
            </svg>
          </div></button>
        </div>
      </form>
    </div>
  </div>
</div>