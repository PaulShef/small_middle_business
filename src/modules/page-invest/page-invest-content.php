<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <a class="breadcrumb-item" href="#">Виды поддержки</a>
  <a class="breadcrumb-item" href="#">Финансовая помощь</a>
  <span class="breadcrumb-item active">Поиск инвестиций</span>
</nav>

<h1>Поиск инвестиций</h1>

<div class="invest">
  <div class="tr">
    <div class="th">Объект инвестиций</div>
    <div class="th">Требуемый объем инвестиций</div>
    <div class="th">Сфера</div>
  </div>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Магазин строительных материалов</div>
    <div class="td">500 000 рублей</div>
    <div class="td">Оптово-розничная торговля</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Продуктовый магазин</div>
    <div class="td">700 000 рублей</div>
    <div class="td">Розничная торговля продуктами питания</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Оптовые продажи нефтепродуктов. Размер инвестиций от 30 000 000 до 100 000 000.</div>
    <div class="td">30 000 000 - 100 000 000 рублей</div>
    <div class="td">Оптовые поставки нефтепродуктов.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Производство</div>
    <div class="td">1 000 000 рублей</div>
    <div class="td">Производство модульных, мобильных зданий, блок-контейнеры, вахтовые поселки. Действующий бизнес.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Магазин строительных материалов</div>
    <div class="td">500 000 рублей</div>
    <div class="td">Оптово-розничная торговля</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Продуктовый магазин</div>
    <div class="td">700 000 рублей</div>
    <div class="td">Розничная торговля продуктами питания</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Оптовые продажи нефтепродуктов. Размер инвестиций от 30 000 000 до 100 000 000.</div>
    <div class="td">30 000 000 - 100 000 000 рублей</div>
    <div class="td">Оптовые поставки нефтепродуктов.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Производство</div>
    <div class="td">1 000 000 рублей</div>
    <div class="td">Производство модульных, мобильных зданий, блок-контейнеры, вахтовые поселки. Действующий бизнес.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Магазин строительных материалов</div>
    <div class="td">500 000 рублей</div>
    <div class="td">Оптово-розничная торговля</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Продуктовый магазин</div>
    <div class="td">700 000 рублей</div>
    <div class="td">Розничная торговля продуктами питания</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Оптовые продажи нефтепродуктов. Размер инвестиций от 30 000 000 до 100 000 000.</div>
    <div class="td">30 000 000 - 100 000 000 рублей</div>
    <div class="td">Оптовые поставки нефтепродуктов.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Производство</div>
    <div class="td">1 000 000 рублей</div>
    <div class="td">Производство модульных, мобильных зданий, блок-контейнеры, вахтовые поселки. Действующий бизнес.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Магазин строительных материалов</div>
    <div class="td">500 000 рублей</div>
    <div class="td">Оптово-розничная торговля</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Продуктовый магазин</div>
    <div class="td">700 000 рублей</div>
    <div class="td">Розничная торговля продуктами питания</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Оптовые продажи нефтепродуктов. Размер инвестиций от 30 000 000 до 100 000 000.</div>
    <div class="td">30 000 000 - 100 000 000 рублей</div>
    <div class="td">Оптовые поставки нефтепродуктов.</div>
  </a>
  <a class="link-reset tr" href="invest-one.php">
    <div class="td">Производство</div>
    <div class="td">1 000 000 рублей</div>
    <div class="td">Производство модульных, мобильных зданий, блок-контейнеры, вахтовые поселки. Действующий бизнес.</div>
  </a>
</div>

<div class="invest-last_questions">Список последних вопросов: 90</div>

<nav aria-label="Page navigation">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg>
    </span>
    </a></li>
    <li class="page-item active"><a class="page-link" href="">1</a></li>
    <li class="page-item"><a class="page-link" href="">2</a></li>
    <li class="page-item"><a class="page-link" href="">3</a></li>
    <li class="page-item"><a class="page-link" href="">4</a></li>
    <li class="page-item"><a class="page-link" href="">5</a></li>
    <li class="page-item"><a class="page-link" href="">6</a></li>
    <li class="page-item"><a class="page-link" href="">7</a></li>
    <li class="page-item"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg>
    </span>
    </a></li>
  </ul>
</nav>