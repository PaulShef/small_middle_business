<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <span class="breadcrumb-item active">Поиск</span>
</nav>

<h1>Результаты поиска</h1>
<section class="search">
  <form class="search__form">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Введите запрос для поиска">
    </div>
    <button type="submit" class="btn btn-1 red">Найти<div class="svg-wrap">
      <svg>
        <use xlink:href="#arrow1"></use>
      </svg>
    </div></button>
  </form>
  <div class="search__result">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla sequi optio laboriosam officiis culpa nesciunt ipsum velit, nostrum non cumque alias hic quaerat, totam molestias mollitia dicta ipsa assumenda consectetur qui. Eum labore veritatis ratione consequatur, ex ipsam quaerat eius. Voluptate quia culpa eius perferendis ad ullam, reprehenderit molestias, officiis incidunt temporibus et, earum repellat dolorem doloribus assumenda, laborum id quo consectetur nulla. Recusandae cupiditate, unde repellat quas asperiores nam repellendus, culpa, consequatur aperiam debitis quidem temporibus beatae. Deleniti, et, facere. Quia molestias minima quaerat, quas. Dignissimos iure provident, voluptatibus perferendis voluptatum at id nobis magnam, atque inventore porro, quas, corporis ex? Minus earum vero aliquid distinctio, veritatis. Amet, quibusdam, animi.
  </div>
</section>