<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <span class="breadcrumb-item active">Поиск</span>
</nav>

<h1>Искать на сайте</h1>
<section class="search">
  <form class="search__form">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Введите запрос для поиска">
    </div>
    <button type="submit" class="btn btn-1 red">Найти<div class="svg-wrap">
      <svg>
        <use xlink:href="#arrow1"></use>
      </svg>
    </div></button>
  </form>
</section>