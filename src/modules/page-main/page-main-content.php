<section class="events">
  <div class="events__title"><a href="" class="link-title">События и мероприятия</a></div>
  <div class="events__gallery">
    <a href="#" class="events__img active">
      <img src="modules/page-main/img/gal-1.jpg" alt="">
    </a>
    <a href="#" class="events__img">
      <img src="modules/page-main/img/gal-2.jpg" alt="">
    </a>
    <a href="#" class="events__img">
      <img src="modules/page-main/img/gal-3.jpg" alt="">
    </a>
    <a href="#" class="events__img">
      <img src="modules/page-main/img/gal-4.jpg" alt="">
    </a>
    <a href="#" class="events__img">
      <img src="modules/page-main/img/gal-5.jpg" alt="">
    </a>
    <a href="#" class="events__img">
      <img src="modules/page-main/img/gal-6.jpg" alt="">
    </a>
  </div>
  <div class="events__arrows">
    <div class="events__arrow events__arrow-prev icon-font disabled fa fa-angle-up"></div>
    <div class="events__arrow events__arrow-next icon-font fa fa-angle-down"></div>
  </div>  
  <div class="events__list-wrap">
    <div class="events__list active">
      <div class="events__item active">
        <a href="news-one.php" class="text-wrap">
          <span class="text ellipsize">111 Предпринимателей приглашают на семинар “Провокатор - Манипулятор”</span>
        </a>
        <div class="date">18.07.2017</div>
        <a href="news-one.php" class="link red"><span class="text">Узнать детали</span></a>
      </div>
      <div class="events__item">
        <a href="news-one.php" class="text-wrap">
          <span class="text ellipsize">222Предпринимателей, заинтересованных в сотрудничестве с Узбекистаном, приглашают принять участие в бизнес-миссии</span>
        </a>
        <div class="date">18.07.2017</div>
        <a href="news-one.php" class="link red"><span class="text">Узнать детали</span></a>
      </div>
      <div class="events__item">
        <a href="news-one.php" class="text-wrap">
          <span class="text ellipsize"> 333 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo maxime ullam praesentium eos ratione suscipit eum harum accusantium voluptatum error, quas similique, at, commodi saepe possimus natus, fuga ex facere officiis nisi cupiditate.</span>
        </a>
        <div class="date">18.07.2017</div>
        <a href="news-one.php" class="link red"><span class="text">Узнать детали</span></a>
      </div>
    </div>
    <div class="events__list collapse">
      <div class="events__item">
       <a href="news-one.php" class="text-wrap">
         <span class="text ellipsize"> 444 Предпринимателей приглашают на семинар “Провокатор - Манипулятор”</span>
       </a>
       <div class="date">18.07.2017</div>
       <a href="news-one.php" class="link red"><span class="text">Узнать детали</span></a>
     </div>
     <div class="events__item">
       <a href="news-one.php" class="text-wrap">
         <span class="text ellipsize"> 555 Предпринимателей, заинтересованных в сотрудничестве с Узбекистаном, приглашают принять участие в бизнес-миссии Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, reprehenderit!</span>
       </a>
       <div class="date">18.07.2017</div>
       <a href="news-one.php" class="link red"><span class="text">Узнать детали</span></a>
     </div>
   </div> 
   <div class="js-events-mobile-toggle"><i class="fa fa-angle-down"></i></div>
 </div>
</section>  

<section class="important"> 
  <div class="important__left">
    <div class="text">Важно</div>
    <div class="dots"></div>
  </div>
  <div class="important__right">
    <div class="icon-font fa fa-exclamation-circle"></div>
    <div class="content">Вниманию предпринимателей! Проводится <strong>опрос по оценке условий ведения бизнеса</strong> в Красноярском крае</div>
    <div class="btn-1 red">
      <div class="text">Узнать детали</div>
      <div class="svg-wrap">
        <svg>
          <use xlink:href="#arrow1"></use>
        </svg>
      </div>
    </div>
  </div>
</section>

<section class="recommend">
  <div class="recommend__left">
    <div class="recommend__title">Рекомендуем</div>
    <div class="recommend__text">Полезные<br>сервисы<br>для бизнеса —</div>
  </div>
  <div class="recommend__right">
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-question"></div>
      <div class="title">Получить<br>бесплатную<br>консультацию</div>
    </a>
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-sitemap"></div>
      <div class="title">Создать<br>мини-сайт</div>
    </a>
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-file-text-o"></div>
      <div class="title">Разместить<br>новость<br>компании</div>
    </a>
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-object-ungroup"></div>
      <div class="title">Воспользоваться<br>бизнес-навигатором<br>от МСП</div>
    </a>
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-th-list"></div>
      <div class="title">Зарегистрироваться<br>в каталоге</div>
    </a>
    <a href="#" class="recommend__item">
      <div class="icon-font fa fa-search-plus"></div>
      <div class="title">Проверить<br>контрагента</div>
      </da 
    </div>
  </section>

  <section class="main_news">
    <div class="main_news__left">
      <div class="main_news__title"><a href="" class="link-title">Новости и объявления</a></div>
      <a class="link-reset main_news__img" href="news-one.php" ><img src="modules/page-main/img/news.jpg" alt=""></a>
      <div class="main_news__text">Новости<br>экономики</div>
      <div class="main_news__dots"></div>
    </div>
    <div class="main_news__right">
      <span href="" class="main_news__subscribe">
        <span class="js-popover-subscribe-btn link red">
          <span class="icon-font fa fa-sign-in"></span>
          <span class="text">Подписаться на новости</span>
        </span>
        <span class="popover-subscribe">
          <form id="subscribe">
            <div class="form-group">
              <label for="subscribe-email">
                <input type="email" pattern="^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$" class="form-control" id="subscribe-email" required>
                <span class="text">E-mail:</span>
              </label>
            </div>
            <button data-timer="3000" data-message="Вы оформили подписку<br>на новости" class="link red"><span class="text">Подписаться</span><div class="svg-wrap">
              <svg>
                <use xlink:href="#arrow1"></use>
              </svg>
            </div>
            </button>
          </form>
        </span>
      </span>
      <div class="main_news__item big">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">За мелкие нарушения не будут строго наказывать</span></a>
        <div class="desc">Административную ответственность бизнеса за нарушения, совершенные впервые и не повлекшие материального ущерба, решено смягчить.</div>
        <a href="news-one.php" class="btn_more red">
          <div class="text">Узнать детали</div><div class="svg-wrap"> <svg> <use xlink:href="#arrow1"></use> </svg> </div>
        </a>
      </div>
      <div class="main_news__item">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">Верните жалобную книгу</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">Собирается ли Роспотребнадзор закрыть крупнейшие торговые сети</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">Курс евро поднимется более чем на 50 процентов</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">Эксперт: Запрет спиртного в выходные приведет к росту алкоголизма</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">17.07.2017 / Объявления</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">Минфин и минтранс разошлись во взглядах на отмену транспортного налога</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">17.07.2017 / Объявления</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">ОБЪЯВЛЕНИЕ о проведении конкурса по отбору муниципальных программ</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
      <div class="main_news__item">
        <div class="date">18.07.2017 / Новости</div>
        <a href="news-one.php" class="title link-mark-all"><span class="text">У России будет общая с ЕАЭС система контроля товаров</span></a>
        <a href="news-one.php" class="link red">Узнать детали</a>
      </div>
    </div>
  </section>

  <section class="map">
    <div class="js-map-view-toggle">
      <i class="fa-angle-up fa"></i>
    </div>
    <div class="map__title">
      <div class="text-1">КРАСНОЯРСКИЙ КРАЙ</div>
      <div class="text-2">Инфраструктура поддержки</div>
      <i class="fa fa-th-list"></i>
    </div>
    <div class="map__main">
      <div class="map__map"></div>
      <div class="map__nav">
        <div class="box">
          <div class="title">Города</div>
          <ul class="list-unstyled">
            <li><a href="cities.php#cities-a">а</a></li>
            <li><a href="cities.php#cities-b">б</a></li>
            <li><a href="cities.php#cities-d">д</a></li>
            <li><a href="cities.php#cities-e">е</a></li>
            <li><a href="cities.php#cities-zh">ж</a></li>
            <li><a href="cities.php#cities-z">з</a></li>
            <li><a href="cities.php#cities-k">к</a></li>
            <li><a href="cities.php#cities-l">л</a></li>
            <li><a href="cities.php#cities-m">м</a></li>
            <li><a href="cities.php#cities-n">н</a></li>
            <li><a href="cities.php#cities-s">с</a></li>
            <li><a href="cities.php#cities-sh">ш</a></li>
          </ul>
        </div>
        <div class="dots"></div>
        <div class="box">
          <div class="title">Районы</div>
          <ul class="list-unstyled">
            <li><a href="">а</a></li>
            <li><a href="">б</a></li>
            <li><a href="">д</a></li>
            <li><a href="">е</a></li>
            <li><a href="">и</a></li>
            <li><a href="">к</a></li>
            <li><a href="">м</a></li>
            <li><a href="">н</a></li>
            <li><a href="">п</a></li>
            <li><a href="">р</a></li>
            <li><a href="">с</a></li>
            <li><a href="">т</a></li>
            <li><a href="">у</a></li>
            <li><a href="">ш</a></li>
            <li><a href="">э</a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="sites">
    <a href="" class="sites__item">
      <span class="sites__img">
        <img src="modules/page-main/img/sites-1.jpg" alt="">
        <img src="modules/page-main/img/sites-1-hover.jpg" alt="" class="hover">
      </span>
      <span class="sites__text">Федеральный портал МСП</span>
    </a>

    <a href="" class="sites__item">
      <span class="sites__img">
        <img src="modules/page-main/img/sites-2.jpg" alt="">
        <img src="modules/page-main/img/sites-2-hover.jpg" alt="" class="hover">
      </span>
      <span class="sites__text">Министерство экономического развития и инвестиционной политики</span>
    </a>

    <a href="" class="sites__item">
      <span class="sites__img">
        <img src="modules/page-main/img/sites-3.jpg" alt="">
        <img src="modules/page-main/img/sites-3-hover.jpg" alt="" class="hover">
      </span>
      <span class="sites__text">Красноярское региональное агентство поддержки МСБ
      </span>
    </a>

    <a href="" class="sites__item">
      <span class="sites__img">
        <img src="modules/page-main/img/sites-4.jpg" alt="">
        <img src="modules/page-main/img/sites-4-hover.jpg" alt="" class="hover">
      </span>
      <span class="sites__text">Правительство Красноярского края</span>
    </a>

    <a href="" class="sites__item">
      <span class="sites__img">
        <img src="modules/page-main/img/sites-5.jpg" alt="">
        <img src="modules/page-main/img/sites-5-hover.jpg" alt="" class="hover">
      </span>
      <span class="sites__text">Официальный сайт РФ для размещения информации о госзакупках</span>
    </a>

  </section>