

<!-- BOOTSTRAP 4 JS -->

<script src="js/vendor/popper.js"></script>
<script src="js/vendor/tooltip.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/chosen.jquery.min.js"></script>



<!-- ======== CUSTOM ======== -->

<script src="js/eventsSliderInit.js"></script>
<script src="js/globals.js"></script>
<script src="js/onDocReady.js"></script>
<script src="js/onWindowLoad.js"></script>
<script src="js/entry.js"></script>