<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <a class="breadcrumb-item" href="#">Виды поддержки</a>
  <span class="breadcrumb-item active">Бесплатные консультации</span>
</nav>

<h1>Бесплатные консультации</h1>

<div class="consult">
  <div class="consult__filter">
    <form>

      <div class="form-group">
        <label for="search">Категория:</label>
        <select class="form-control" id="search">
          <option>Все</option>
          <option>Бухгалтерский учет и налогообложение</option>
          <option>Разное (только по вопросам предпринимательства)</option>
          <option>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, vel!</option>
          <option>Lorem ipsum dolor sit amet.</option>
        </select>
      </div>

      <div class="form-group">
        <label for="search">Ключевые слова:</label>
        <input type="text" class="form-control" id="search">
      </div>

      <div class="action-btns">
        <button type="submit" class="btn btn-1 red">
          <div class="text">Найти</div>
          <div class="svg-wrap">
            <svg>
              <use xlink:href="#arrow1"></use>
            </svg>
          </div>
        </button>
        или 
        <span class="clear-filter">сбросить фильтр</span>
      </div>
    </form>
  </div>
  <table class="consult__table">
    <tr>
        <th>Дата вопроса</th>
        <th>Краткое содержание</th>
      <th>Категория</th>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Возможно ли предоставление ежегодного оплачиваемого отпуска с выходного дня? ...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Какие расходы можно проводить как представительские?</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте. Я хочу оказывать парикмахерские услуги. Могу ли я работать на патенте?...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Как можно получить разрешение на размещение мобильной точки...</a></td>
      <td>Разное (только по вопросам предпринимательства)</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте! Какие расходы относятся к расходам на оплату труда при расчете налога...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Возможно ли предоставление ежегодного оплачиваемого отпуска с выходного дня? ...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Какие расходы можно проводить как представительские?</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте. Я хочу оказывать парикмахерские услуги. Могу ли я работать на патенте?...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Как можно получить разрешение на размещение мобильной точки...</a></td>
      <td>Разное (только по вопросам предпринимательства)</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте! Какие расходы относятся к расходам на оплату труда при расчете налога...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Возможно ли предоставление ежегодного оплачиваемого отпуска с выходного дня? ...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Какие расходы можно проводить как представительские?</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте. Я хочу оказывать парикмахерские услуги. Могу ли я работать на патенте?...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Как можно получить разрешение на размещение мобильной точки...</a></td>
      <td>Разное (только по вопросам предпринимательства)</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте! Какие расходы относятся к расходам на оплату труда при расчете налога...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Возможно ли предоставление ежегодного оплачиваемого отпуска с выходного дня? ...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Какие расходы можно проводить как представительские?</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте. Я хочу оказывать парикмахерские услуги. Могу ли я работать на патенте?...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Добрый день! Как можно получить разрешение на размещение мобильной точки...</a></td>
      <td>Разное (только по вопросам предпринимательства)</td>
    </tr>
    <tr>
      <td>04-12-2016</td>
      <td><a class="link-reset" href="invest-one.php">Здравствуйте! Какие расходы относятся к расходам на оплату труда при расчете налога...</a></td>
      <td>Бухгалтерский учет и налогообложение</td>
    </tr>
  </table>
  <div class="consult__last_questions">Список последних вопросов: 90</div>
</div>



<nav aria-label="Page navigation">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg>
    </span>
  </a></li>
  <li class="page-item active"><a class="page-link" href="">1</a></li>
  <li class="page-item"><a class="page-link" href="">2</a></li>
  <li class="page-item"><a class="page-link" href="">3</a></li>
  <li class="page-item"><a class="page-link" href="">4</a></li>
  <li class="page-item"><a class="page-link" href="">5</a></li>
  <li class="page-item"><a class="page-link" href="">6</a></li>
  <li class="page-item"><a class="page-link" href="">7</a></li>
  <li class="page-item"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg>
  </span>
</a></li>
</ul>
</nav>