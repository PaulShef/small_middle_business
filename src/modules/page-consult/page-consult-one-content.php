<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <a class="breadcrumb-item" href="#">Виды поддержки</a>
  <span class="breadcrumb-item active">Бесплатные консультации</span>
</nav>

<h1>Бухгалтерский учет и налогообложение</h1>

<section class="consult-one">
  <div class="consult-one__title">Вопрос (24.09.17 15:21) - Денисова Елена</div>
  <div class="consult-one__content">Добрый день. При оказании разовых услуг для ООО возник вопрос минимизации налогов при расчете за оказанные услуги. Поясните, пожалуйста, какие налоги я заплачу по договору подряда как физлицо, и какие налоги заплачу при договоре с ООО как ИП?</div>
  <div class="consult-one__title">Ответ (27.09.17 02:36) - Консультант 4</div>
  <div class="consult-one__content">Добрый день, Елена!<br>
Возможно, что не правильно Вас поняла. Если что, уточняйте еще раз в этом же сообщении.<br>
1. Если Вы являетесь физлицом и оказываете услугу для ООО. При этом ООО заключает с Вами договор подряда. В этом случае с суммы вознаграждения по договору организация удержит у Вас налог на доходы физлиц и перечислит в бюджет. С Вас больше налогов не возьмут, а вот организация еще заплатит пенсионные взнос, взнос на мед, страхование с начисленной Вам суммы. Взнос в ПФР отразится в Вашем лицевом счете в ПФР.<br>
2. Если Вы - индивидуальный предприниматель и заключили договор оказания услуг с ООО. ВАш налог будет зависеть от того, на какой системе налогообложения вы, как ип находитесь. Если на общей - 13%, на УСН (доходы) - 6% (и их можно уменьшить на фиксированный взнос), УСН (доходы минус расходы) - 15%.<br>
Кроме этого, если Ваш годовой доход более 300000,00 рублей, то еще 1% в ПФР.</div>
</section>