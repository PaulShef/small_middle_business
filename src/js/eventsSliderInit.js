var timeoutId;
function eventsSliderInit() {

  var $mainEvents = $('.events');
  if ($mainEvents.length != 0) {
    var $i = $mainEvents.find('.events__item'),
    $iMax = $i.length,
    $img = $mainEvents.find('.events__img'),
    $arrP = $mainEvents.find('.events__arrow-prev'),
    $arrN = $mainEvents.find('.events__arrow-next'),
    $iWrap = $mainEvents.find('.events__list'),
    $iActive;

    // активируем лист с активным элементом
    function events__list($iActive) {
      if (($iActive + 1) / 3 > 1) {
        $iWrap.eq(1).addClass('active');
        $iWrap.eq(0).removeClass('active');
      } else {
        $iWrap.eq(0).addClass('active');
        $iWrap.eq(1).removeClass('active');
      }
    }

    // переключаем картинку
    function events__img($iActive) {
      $img.removeClass('active');
      $img.eq($iActive).addClass('active');
    }

    // Настраиваем таймер
    var start = function() {
      clearInterval(timeoutId);
      timeoutId = setInterval(function() {
        $i.each(function(i, t) {
          if ($(t).hasClass('active')) {
            $iActive = i;
            $(t).removeClass('active')
          }
        });

        if ($iActive + 1 < $iMax) {
          $i.eq($iActive + 1).addClass('active');
          $iActive++;
        } else {
          $i.eq(0).addClass('active');
          $iActive = 0;
        };

        events__list($iActive);
        events__img($iActive);
        arrDis($iActive);
      }, 5000);
    }

    // Ставим на паузу таймер при наведении мыши
    var stop = function() {
      clearInterval(timeoutId);
    }

    start();

    $('.events').mouseenter(function() {
      stop();
    });
    $('.events').mouseleave(function() {
      start();
    });

    // Подключаем кнопки управления след / пред

    $arrP.on('click', function() {
      setTimeout(function() {

        $i.each(function(i, t) {
          if ($(t).hasClass('active')) {
            $iActive = i;
            $(t).removeClass('active')
            $i.eq($iActive - 1).addClass('active');
            $iActive--;
            return false;
          }
        });


        events__list($iActive)
        events__img($iActive)
        arrDis($iActive)
      }, 200);
    })

    $arrN.on('click', function() {
      setTimeout(function() {
        $i.each(function(i, t) {
          if ($(t).hasClass('active')) {
            $iActive = i;
            $(t).removeClass('active')
            $i.eq($iActive + 1).addClass('active');
            $iActive++;
            return false;
          }
        });


        events__list($iActive)
        events__img($iActive)
        arrDis($iActive)
      }, 200);
    })

    // блокируем кнопки на первом / последнем элементах

    function arrDis($iActive) {
      if ($iActive == 0) {
        $arrP.addClass('disabled');
        $arrN.removeClass('disabled');
      } else if ($iActive == $iMax - 1) {
        $arrN.addClass('disabled');
        $arrP.removeClass('disabled');
      } else {
        $arrN.removeClass('disabled');
        $arrP.removeClass('disabled');
      }
    }
  }
}

function eventsMobileInit()  {
  clearInterval(timeoutId);
  // console.log('eventsMobileInit')
  var $mainEvents = $('.events');
  if ($mainEvents.length != 0) {
    var $i = $mainEvents.find('.events__item'),
    $iWrap = $mainEvents.find('.events__list');

    $i.removeClass('active');
    // $iWrap.removeClass('active');
    if (($i.length > 3) && ($iWrap.length > 1)) {
      $('.js-events-mobile-toggle').click(function () {
        $R = $(this);
        if (!$R.hasClass('active')) {
          $iWrap.eq(1).collapse('show');
          $R.addClass('active');
        } else {
          $iWrap.eq(1).collapse('hide');
          $R.removeClass('active');
        }
      })
    }
  }
}