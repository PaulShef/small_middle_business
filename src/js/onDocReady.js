function onDocReady() {

  // console.log('RUN onDocReady()');

  var $cities = $('.cities');
  if ($cities.length != 0) {
    $cities.find('.cities__name').on('click', function() {
      if ($(this).closest('.cities__item').hasClass('description-open')) {
        $(this).closest('.cities__item').removeClass('description-open').find('.cities__description').collapse('hide');
      } else {
        $(this).closest('.cities__item').addClass('description-open').find('.cities__description').collapse('show');
      }
    })
    $('.cities-nav a').on('click', function(e) {
      var $R = $(this).attr('href');
      $('html,body').stop().animate({ scrollTop: $($R).offset().top }, 1000);
      e.preventDefault();
    });
  }


  // init Chosen select
  $("select:not(.no-search)").chosen({ disable_search_threshold: 10 });
  $("select.no-search").chosen({ disable_search_threshold: 999 });
  $("select").on('change', function() {
    $(this).trigger("chosen:updated")
  })

  // Переключение вкладок входа в личный кабинет
  if ($(".lk__toggle_nav").length != 0) {
    $('.js-login').click(function() {
      $(this).closest('.lk').addClass('open-login').removeClass('open-signin');
    })
    $('.js-signin').click(function() {
      $(this).closest('.lk').addClass('open-signin').removeClass('open-login');
    })
    $('#js-reg-check').on('change', function() {
      console.log('0')

      if ($(this).find('input').is(':checked')) {
        $(this).closest('form').find('[type="submit"]').removeClass('disabled')
      } else {
        $(this).closest('form').find('[type="submit"]').addClass('disabled')
      }
    })
  }

  // кнопка мобильного меню (с отключением скролла на body)
  $('.aside__btn_menu').click(function() {
    if ($body.hasClass('menu-open')) {

      $body.css({ 'overflow': 'visible' }).removeClass('menu-open');
    } else {

      $body.css({ 'overflow': 'hidden' }).addClass('menu-open');
    }
  })

  // Открываем подменю в главном меню
  $('.js-submenu').click(function() {
    if (window.innerWidth < 1280) {

      if ($(this).hasClass('active')) {
        $(this).removeClass('active').parent().children('ul').collapse('hide');
      } else {
        $(this).addClass('active').parent().children('ul').collapse('show');
      }
    } else {
      $(this).addClass('active').closest('ul').addClass('hidden');
    }
  })


  // Закрываем подменю в главном меню
  $('.js-close-submenu').click(function() {
    $(this).closest('ul').prevAll('.js-submenu').removeClass('active').closest('ul').removeClass('hidden')
  })


  // Всплывашка для подписки на новости
  $('.js-popover-subscribe-btn').click(function() {
    $(this).toggleClass('open-popover');
  })


  $('.js-map-view-toggle').click(function() {
    $(this).closest('section.map').toggleClass('map-hidden');
  })


  $('.js-page-scroll-to-top').click(function(e) {
    $('html,body').stop().animate({ scrollTop: 0 }, 1000);
    e.preventDefault();
  });



  $win.on('scroll', function() {
    if ($win.scrollTop() > 190) {
      $('.js-page-scroll-to-top').addClass('show');
    } else {
      $('.js-page-scroll-to-top').removeClass('show');
    }
  })

  var $subscribe = $('#subscribe');

  // $subscribe

  $subscribe.find('button').click(function(e) {

    e.preventDefault();
    var pattern = new RegExp("^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$");
    if (pattern.test($subscribe.find('input').val())) {
      var mess = $(this).data('message'),
        timer = $(this).data('timer'),
        temp = '<div class="info-message"><div class="info-message__content">' +
        mess +
        '</div> </div>',

        info = $('footer').before(temp).prev();
      setTimeout(function() {
        info.addClass('show');
        setTimeout(function() {
          info.removeClass('show');
          setTimeout(function() {
            info.remove();
          }, 1000)
        }, timer);
      }, 100);
      console.log('valid')
    } else { console.log('invalid') }


  });


  function ellipsizeTextBox(id) {
    var el = $(id);
    el.each(function(i, t) {
      var wordArray = t.innerHTML.split(' ');

      while (t.scrollHeight > parseInt($(t).css('max-height').replace('px', ''))) {
        wordArray.pop();
        t.innerHTML = wordArray.join(' ') + '...';
      }

      setTimeout(function() {
        $(t).addClass('min')
      }, 500)
    })
  }

  ellipsizeTextBox('.ellipsize');

  var eventsInit = function () { if (window.innerWidth > 1023) { eventsSliderInit() } else { eventsMobileInit() } }
  eventsInit();
  resizeHandlers.push(eventsInit);
}